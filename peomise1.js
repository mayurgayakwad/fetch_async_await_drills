/* Problem:1
Create a function named getFirstName that returns a promise. 
When the promise is resolved it will return a value of {name: "Arya"}. */

let getFirstName = new Promise( function (resolve,reject) {
    let obj = {name: "Arya"}
    return resolve(obj.name)
})

getFirstName.then((value) => {
    console.log(value)
} )