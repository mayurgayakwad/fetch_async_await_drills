/* Problem3:
Create another function named getFullName which returns a promise. 
And when the promise is resolved it will return the full name that 
you will get by resolving the promises from the above two functions.*/

let getFirstName = new Promise( function (resolve,reject) {
    let obj = {name: "Arya"}
    return resolve(obj.name)
})

let getLastName = new Promise((resolve,reject) => {
    let obj = {name: "Stark"}
    return resolve(obj.name)
} )

 function getFullName(){
        return Promise.all([getFirstName,getLastName])
}
getFullName().then((value) => {
    console.log(value[0] + ' ' +value[1])
})