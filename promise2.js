/* Problem2:
Create a function named getLastName that returns a promise. 
When the promise is resolved it will return a value of {name: "Stark"}. */

let getLastName = new Promise((resolve,reject) => {
    let obj = {name: "Stark"}
    return resolve (obj.name)
} )

getLastName.then((value) => {
    console.log(value)
})