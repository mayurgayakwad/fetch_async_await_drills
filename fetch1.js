/* Problem1: Create a function for fetching the data using 
the below URL and console the first 10 values of data. ( Using promise ) */


fetch = require('node-fetch')
function getFetchData () {
return new Promise((resolve, reject)=>{
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((resp) => resp.json())
    .then((data) => resolve(data.slice(0,10)))
})
 
}
getFetchData().then((value) => console.log(value))