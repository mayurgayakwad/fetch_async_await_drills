// Problem2: Write the same function using async & await for fetching the data. ( Using async & await )
fetch = require('node-fetch')
async function getFetchData() {
	const response = await fetch('https://jsonplaceholder.typicode.com/users')
	const data = await response.json(); // Extracting data as a JSON Object from the response
    console.log(data.slice(0,10))
}

getFetchData()